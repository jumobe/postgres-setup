create user test_user with encrypted password 'test_pass';

grant all privileges on database test_db to test_user;
