-- https://www.postgresql.org/docs/9.1/sql-createtable.html


CREATE TABLE films (
    code        char(5) CONSTRAINT firstkey PRIMARY KEY,
    title       varchar(40) NOT NULL,
    did         integer NOT NULL,
    date_prod   date,
    kind        varchar(10),
    len         interval hour to minute
);


insert into films (code,
                   title,
                   did,
                   date_prod,
                   kind,
                   len)
           values ('ACD',                         -- code 
                   'film title',                  -- title
                   9,                             -- did
                   now(),                         -- date_prod
                   'terror',                      -- kind
                   INTERVAL '4 hours 3 minutes'); -- len
