#!/bin/bash

PG_VERSION='12.4'

set -e

wget -c https://ftp.postgresql.org/pub/source/v${PG_VERSION}/postgresql-${PG_VERSION}.tar.bz2

tar -xvjf postgresql-${PG_VERSION}.tar.bz2

mkdir postgres

INSTALL_DIR=${PWD}/postgres

cd postgresql-${PG_VERSION}

mkdir compil

cd compil

../configure --prefix=${INSTALL_DIR} --without-readline

make -j 2

make install

cd $INSTALL_DIR

mkdir -p ${INSTALL_DIR}/data

${INSTALL_DIR}/bin/initdb -D ${INSTALL_DIR}/data

cd ..

#
# start
#
${INSTALL_DIR}/bin/pg_ctl -D ${INSTALL_DIR}/data -l logfile start

${INSTALL_DIR}/bin/createdb test_db

${INSTALL_DIR}/bin/psql -d test_db -a -f setup_user.sql

${INSTALL_DIR}/bin/psql -U test_user -d test_db -a -f setup.sql

#
# stop
#
# ${INSTALL_DIR}/bin/pg_ctl -D ${INSTALL_DIR}/data stop


#
# Allow remote access with pass
#
# emacs data/postgresql.conf
#    line: listen_addresses = '*'    # what IP address(es) to listen on;
#
# emacs data/pg_hba.conf
#    line: host    all     all       0.0.0.0/0      password
#


# ./postgres/bin/createuser test_user

# ./postgres/bin/createdb test_db

# List databases: \l
# User database: \c <db_name>
# List tables: \dt
# Exit: \q


# Create user:  create user test_user with encrypted password 'test_pass';
# Grant to user: grant all privileges on database test_db to test_user;


#
# Execute command line inteface
#
# export LD_LIBRARY_PATH="${INSTALL_DIR}/lib:$LD_LIBRARY_PATH"; ${INSTALL_DIR}/bin/psql test_db
